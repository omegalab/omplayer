#include "file_tree.h"

#include <iostream>
#include <stdexcept>

#include <dirent.h>
#include <sys/stat.h>

#include "fmt/format.h"

#include "utils/logger.h"

FileSystemTree::FileSystemTree() {}

/*!
 * \brief walks through directory or originates file
 * \param item filesystem item passed by FileSystemTree::ReadDir function
 */

void FileSystemTree::WalkThrough(FileSystemItem& item) {
  struct stat unix_file_stat;

  if (::stat(item.path.c_str(), &unix_file_stat) == 0) {
    if (unix_file_stat.st_mode & S_IFREG)
      OriginateFile(item);
    else if (unix_file_stat.st_mode & S_IFDIR)
      ReadDir(item.path);
    else
      throw std::runtime_error("WalkThrough unix_file_stat");

  } else
    throw std::runtime_error(fmt::format("WalkThrough {}", item.path));
}

/*!
 * \brief reads directory content and executes FileSystemTree::WalkThrough func
 * for each item.
 * \param path directory filesystem path.
 */

void FileSystemTree::ReadDir(const std::string& path) {
  DIR* dirp = opendir(path.c_str());

  if (dirp == nullptr)
    throw std::runtime_error(fmt::format("no such directory: {}", path));

  struct dirent* dp;

  std::vector<FileSystemItem> items;

  while ((dp = readdir(dirp)) != nullptr) {
    std::string tmp(dp->d_name);

    if (tmp != "." && tmp != "..")
      items.emplace_back(FileSystemItem{fmt::format("{}/{}", path, tmp), tmp, {}});
  }

  closedir(dirp);

  for (auto&& file_system_item : items) {
    WalkThrough(file_system_item);
  }
}

/*!
 * \brief detect file type and creates corresponding CodeFileBase object
 * \param item file struct to anylize
 */

void FileSystemTree::OriginateFile(FileSystemItem& item) {
  size_t pos = item.name.find_last_of('.');

  if (pos < item.name.size() - 1)
    item.extension = item.name.substr(pos + 1);

  if (!item.extension.empty())
    files_.push_back(item);
}

/*!
 * \brief clears internal CodeFileBase objects container
 */
void FileSystemTree::Clear() {
  files_.clear();
}

std::vector<FileSystemItem>& FileSystemTree::GetFiles() {
  return files_;
}
