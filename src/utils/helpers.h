#ifndef HELPERS_H
#define HELPERS_H

#include <cstddef>
#include <sstream>
#include <string>

template <typename It>
std::string PrintRange(const It& begin, const It& end) {
  std::ostringstream os;
  for (auto it = begin; it != end; ++it) {
    os << *it;
  }
  return os.str();
}

namespace omega {
#if __x86_64__
constexpr unsigned long _hash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (_hash(str, h + 1) * 33) ^ static_cast<size_t>(str[h]);
}

constexpr auto operator"" _(char const* p, unsigned long) {
  return _hash(p);
}
#else
constexpr unsigned _hash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (_hash(str, h + 1) * 33) ^ static_cast<uint32_t>(str[h]);
}

constexpr auto operator"" _(char const* p, unsigned) {
  return _hash(p);
}
#endif

}  // namespace omega

#endif  // HELPERS_H
