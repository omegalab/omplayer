#include "options.h"

#include <algorithm>
#include <filesystem>
#include <fstream>

#include "fmt/format.h"

#include "omplayer.h"
#include "utils/logger.h"

namespace omega {

void Options::ReadJson(const std::string& config_path) {
  std::ifstream json_file(config_path);

  if (json_file.is_open())
    json_file >> data_;

  else
    throw std::runtime_error(fmt::format("failed open json file {}", config_path));
}

void Options::ValidateJson() {
  if (data_.find("id") == data_.end())
    throw std::runtime_error("no pi id in config file");
  if (data_.find("serial") == data_.end())
    throw std::runtime_error("no seiral settings id in config file");
}

void Options::SortFileList() {
  try {
    using fs = std::filesystem::path;

    std::sort(std::begin(files_), std::end(files_), [](const std::string& lhs, const std::string& rhs) {
      return std::stoi(fs(lhs).filename().stem()) < std::stoi(fs(rhs).filename().stem());
    });

  } catch (std::invalid_argument&) {
    throw(std::invalid_argument("symbolic filename in video directory"));
  }
}

Options::Options(int argc, char* argv[]) {
  if (argc < 2)
    throw std::runtime_error(fmt::format("usage: {} [config file] <source file>", PROJECT_NAME));

  for (int i = 0; i < argc; i++)
    argv_.emplace_back(argv[i]);
}

nlohmann::json& Options::Get() {
  return data_;
}

void Options::Validate() {
  ReadJson(argv_[1]);
  ValidateJson();

  if (argv_.size() == 2) {
    auto fs = std::make_shared<FileSystemTree>();

    fs->ReadDir(data_.at("source").get<std::string>());

    for (auto& item : fs->GetFiles())
      files_.push_back(item.path);
  } else
    files_.push_back(argv_[2]);

  SortFileList();
}

std::vector<std::string>& Options::GetFiles() {
  return files_;
}

}  // namespace omega
