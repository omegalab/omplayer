#ifndef OPTIONS_H
#define OPTIONS_H

#include "nlohmann/json.hpp"
#include "utils/file_tree.h"

namespace omega {

class Options {
 public:
  Options(int argc, char* argv[]);

  nlohmann::json& Get();
  void GetSourceFromConfig();
  void Validate();
  std::vector<std::string>& GetFiles();

  auto operator[](const std::string& key) {
    return data_[key];
  }

  auto Find(const std::string& key) {
    return data_.find(key);
  }

  auto End() {
    return data_.end();
  }

 private:
  void ReadJson(const std::string&);
  void ValidateJson();
  void SortFileList();

  nlohmann::json data_;
  std::vector<std::string> files_;
  std::vector<std::string> argv_;
};

}  // namespace omega

#endif  // OPTIONS_H
