#ifndef FILETREE_H
#define FILETREE_H

#include <string>
#include <vector>

struct FileSystemItem {
  std::string path;
  std::string name;
  std::string extension;
};

/*!
 * \brief The FileSystemTree class
 *
 * recursively reads directories and  creates corresponging CodeFile objects for
 * each file.
 */

class FileSystemTree {
 public:
  FileSystemTree();

  void WalkThrough(FileSystemItem&);
  void ReadDir(const std::string&);
  void OriginateFile(FileSystemItem& item);
  void Clear();

  std::vector<FileSystemItem>& GetFiles();

 private:
  std::vector<FileSystemItem> files_;
};

#endif  // FILETREE_H
