#ifndef PLAYER_H
#define PLAYER_H

#include <signal.h>
#include <memory>

#include <QApplication>
#include <QObject>
#include <QSocketNotifier>

#include "vlcpp/vlc.hpp"

#include "base_controller.h"
#include "gpio.h"
#include "mainwindow.h"
#include "serial_port.h"

#include "io/io.h"
#include "io/queue.h"

#include "omplayer.h"
#include "utils/options.h"

namespace omega {

class OmPlayer : public QApplication {
  Q_OBJECT
 public:
  enum class Status { IDLE, OPENING, PLAYING, PAUSED, STOPPING, ENDED, ERROR };
  enum class ControlMode { SERIAL = 0, GPIO };

  OmPlayer(int& argc, char* argv[], Options&);
  ~OmPlayer();

  void Start();
  void Stop();
  void GpioSwitch(int, int);

  static void TermSignalHandler(int unused);
  static void IntSignalHandler(int unused);

  friend inline std::ostream& operator<<(std::ostream& os, const OmPlayer&) {
    return os << "version: " << PROJECT_VER << " build date: " << PROJECT_BUILD_DATE;
  }

 public slots:
  void HandleSigInt();
  void HandleSigTerm();

 private:
  const std::vector<std::string> kStatus{"IDLE", "OPENING", "PLAYING", "PAUSED", "STOPPING", "ENDED", "ERROR"};

  static int sig_int_fd[2];
  static int sig_term_fd[2];

  QSocketNotifier* sn_int;
  QSocketNotifier* sn_term;

  MainWindow* main_window_;
  Options& opts_;
  Io io_;

  ControlMode mode_;
  BaseController::Ptr controller_;

  std::unique_ptr<std::thread> player_thread_;
  std::unique_ptr<VLC::Instance> instance_;
  std::shared_ptr<VLC::MediaPlayer> base_player_;
  std::shared_ptr<VLC::MediaListPlayer> player_;
  std::shared_ptr<VLC::MediaList> play_list_;
  libvlc_time_t current_pos_;

  void ParseCommand(const std::string&);
  void Control(const std::string& cmd, const std::string& arg);

  void StartInSerialMode();
  void StartInGpioMode();

 private slots:

  void ToggleLoopMode(int);
  void ToggleMute(bool);
  void SetPosistion(int);
  void ToggleFullScreen(bool);

 signals:
  void onToggleFullScreen(bool);
  void onToggleLoopMode(int);
  void onToggleMute(bool);
  void onPlayBackStarted();
  void onPlaybackStopped();
  void onSetPosistion(int);
};

}  // namespace omega

#endif  // PLAYER_H
