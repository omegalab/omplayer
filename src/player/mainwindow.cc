#include "mainwindow.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QStyle>

#include "utils/logger.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
  setCentralWidget(new QWidget());
  video_widget_ = new QWidget(this);

  QHBoxLayout* layout = new QHBoxLayout();
  layout->setMargin(0);
  layout->addWidget(video_widget_);

  centralWidget()->setLayout(layout);
  restore_show_ = std::bind(&MainWindow::showNormal, this);
}

void MainWindow::SetupWindow() {
  video_widget_->setAutoFillBackground(true);
  QPalette plt = palette();
  plt.setColor(QPalette::Window, Qt::black);
  video_widget_->setPalette(plt);

  QRect availableGeometry = QApplication::desktop()->availableGeometry();
  QSize availableSize = availableGeometry.size();

  int screenWidth = availableSize.width();
  int screenHeight = availableSize.height();

  QSize newSize(static_cast<int>(screenWidth * 0.8), static_cast<int>(screenHeight * 0.8));
  QRect newGeometry = QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, newSize, availableGeometry);

  setGeometry(newGeometry);
  setWindowTitle("OmPlayer");
}

uint32_t MainWindow::GetWidgetId() const {
  return static_cast<uint32_t>(video_widget_->winId());
}

void MainWindow::closeEvent(QCloseEvent* event) {
  emit onClose();
  event->accept();
}

void MainWindow::keyPressEvent(QKeyEvent* event) {
  if (event->key() == Qt::Key_Escape) {
    FullScreen(false);
    emit onKeyEscape();
  }
}

void MainWindow::FullScreen(bool fs) {
  if (fs) {
    showFullScreen();
    restore_show_ = std::bind(&MainWindow::showFullScreen, this);
  } else {
    showNormal();
    restore_show_ = std::bind(&MainWindow::showNormal, this);
  }
}

void MainWindow::Show() {
  if (isHidden())
    restore_show_();
}

void MainWindow::Hide() {
  if (isVisible())
    hide();
}
