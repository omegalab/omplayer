#include "serial_port.h"

#include <fcntl.h>
#include <termios.h>

#include "fmt/format.h"
#include "io/handler.h"

static const std::map<int, speed_t> kBauds{                    //
                                           {4800, B4800},      //
                                           {9600, B9600},      //
                                           {19200, B19200},    //
                                           {38400, B38400},    //
                                           {57600, B57600},    //
                                           {115200, B115200},  //
                                           {230400, B230400},  //
                                           {460800, B460800}};

namespace omega {

SerialPort::SerialPort(Io& io) : io_(io) {}

void SerialPort::Stop() {
  close(fd_);
}

void SerialPort::StartImpl(SerialPortHandler::Sig&& func) {
  AsyncAccept(std::move(func));
}

void SerialPort::InitImpl(const std::string& dev, int speed) {
  fd_ = open(dev.c_str(), O_RDWR);

  if (-1 == fd_)
    throw std::runtime_error(fmt::format("failed opening serial port {}", dev));

  SetSpeed(speed);
}

void SerialPort::AsyncAccept(SerialPortHandler::Sig&& f) {
  auto handler = std::make_shared<SerialPortHandler>(std::move(f));

  handler->e.data.fd = fd_;
  handler->e.events = EPOLLIN | EPOLLET;

  io_.Register(handler);
}

void SerialPort::SetSpeed(int speed) {
  struct termios opts;
  tcgetattr(fd_, &opts);
  cfsetispeed(&opts, kBauds.at(speed));
  cfsetospeed(&opts, kBauds.at(speed));
  tcsetattr(fd_, TCSANOW, &opts);
}

}  // namespace omega
