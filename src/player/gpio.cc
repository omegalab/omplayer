#include "gpio.h"

#include <algorithm>
#include <chrono>

#include <wiringPi.h>

#include "fmt/format.h"

#include "utils/logger.h"

namespace omega {

std::map<int, int> GPIO::bcm_to_wiring{{4, 7},   {17, 0},  {27, 2},  {22, 3},  {5, 21}, {6, 22},
                                       {13, 23}, {19, 24}, {26, 25}, {18, 1},  {23, 4}, {24, 5},
                                       {25, 6},  {12, 26}, {16, 27}, {20, 28}, {21, 29}};

GPIO::GPIO() {
  if (wiringPiSetup() == -1)
    throw(std::runtime_error("error open GPIO"));
}

GPIO::~GPIO() {
  if (gpio_thread_ && gpio_thread_->joinable())
    gpio_thread_->join();
}

void GPIO::InitImpl(const std::vector<int>& pins) {
  for (auto& bcm : pins) {
    if (!bcm_to_wiring.count(bcm))
      throw(std::logic_error(fmt::format("invalid pin: {}", bcm)));

    int wiring_pin = bcm_to_wiring.at(bcm);
    pinMode(wiring_pin, INPUT);

    pins_[wiring_pin] = digitalRead(wiring_pin);
  }
}

void GPIO::StartImpl(CallBackFunc&& callback) {
  gpio_thread_ = std::make_unique<std::thread>([&, callback]() {
    while (!stopped_) {
      std::this_thread::sleep_for(std::chrono::seconds(1));

      ReadAllPins(callback);
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  });
}

void GPIO::ReadAllPins(const CallBackFunc& callback) {
  for (auto it = pins_.begin(); it != pins_.end(); it++) {
    int new_state = digitalRead(it->first);

    if (new_state != it->second) {
      int clip_index = std::distance(std::begin(pins_), it);
      callback(clip_index, new_state);
      it->second = new_state;
    }
  }
}

void GPIO::Stop() {
  stopped_ = true;
}

}  // namespace omega
