#ifndef OMEGA_GPIO_H
#define OMEGA_GPIO_H

#include <map>
#include <thread>

#include "base_controller.h"

namespace omega {

class GPIO : public BaseController {
 public:
  using Ptr = std::shared_ptr<GPIO>;
  using CallBackFunc = std::function<void(int, int)>;

  GPIO();
  ~GPIO() override;

  void Stop() override;

 protected:
  void InitImpl(const std::vector<int>&) override;
  void StartImpl(CallBackFunc&&) override;

 private:
  static std::map<int, int> bcm_to_wiring;

  bool stopped_ = false;
  int curr_clip_ = -1;

  std::map<int, int> pins_;
  std::unique_ptr<std::thread> gpio_thread_;

  void ReadAllPins(const CallBackFunc&);
};

}  // namespace omega

#endif  // OMEGA_GPIO_H
