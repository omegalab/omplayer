#include "base_controller.h"

#include "utils/logger.h"

namespace omega {

BaseController::~BaseController() {}

void BaseController::StartImpl(std::function<void(int, int)>&&) {
  LOG(WARN) << "not implemented!";
}

void BaseController::StartImpl(SerialPortHandler::Sig&&) {
  LOG(WARN) << "not implemented!";
}

}  // namespace omega
