#ifndef OMEGA_BASECONTROLLER_H
#define OMEGA_BASECONTROLLER_H

#include <functional>
#include <memory>

#include "io/handler.h"

namespace omega {

class BaseController {
 public:
  using Ptr = std::shared_ptr<BaseController>;

  BaseController() = default;
  virtual ~BaseController();

  template <typename... Args>
  void Init(Args... args) {
    InitImpl(std::forward<Args>(args)...);
  }

  template <typename Func>
  void Start(Func&& func) {
    StartImpl(std::move(func));
  }

  virtual void Stop() {}

 protected:
  virtual void StartImpl(std::function<void(int, int)>&&);
  virtual void StartImpl(SerialPortHandler::Sig&&);

  virtual void InitImpl(const std::string&, int) {}
  virtual void InitImpl(const std::vector<int>&) {}
};

}  // namespace omega

#endif  // OMEGA_BASECONTROLLER_H
