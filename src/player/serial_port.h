#ifndef SERIAL_PORT_H
#define SERIAL_PORT_H

#include <string>

#include "base_controller.h"

#include "io/handler.h"
#include "io/io.h"

namespace omega {

class SerialPort : public BaseController {
 public:
  using Ptr = std::shared_ptr<SerialPort>;

  SerialPort(Io&);

  void Stop() override;

 protected:
  void InitImpl(const std::string&, int speed) override;
  void StartImpl(SerialPortHandler::Sig&& func) override;

 private:
  Io& io_;
  int fd_;

  void SetSpeed(int);
  void AsyncAccept(SerialPortHandler::Sig&& func);
};

}  // namespace omega

#endif  // SERIAL_PORT_H
