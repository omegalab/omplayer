#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <functional>

#include <QCloseEvent>
#include <QMainWindow>
#include <QSplitter>
#include <QVBoxLayout>

class MainWindow : public QMainWindow {
  Q_OBJECT
 public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow() = default;

  void SetupWindow();
  uint32_t GetWidgetId() const;

 protected:
  virtual void closeEvent(QCloseEvent*);
  virtual void keyPressEvent(QKeyEvent*);

 public slots:
  void FullScreen(bool fs);
  void Show();
  void Hide();

 private:
  QWidget* video_widget_;
  std::function<void()> restore_show_;

 signals:
  void onClose();
  void onKeyEscape();
};

#endif  // MAINWINDOW_H
