#include "player.h"

#include <filesystem>
#include <sstream>
#include <vector>

#include "fmt/format.h"

#include "utils/helpers.h"
#include "utils/logger.h"

namespace omega {

int OmPlayer::sig_int_fd[2];
int OmPlayer::sig_term_fd[2];

OmPlayer::OmPlayer(int& argc, char* argv[], Options& opts) : QApplication(argc, argv), opts_(opts), io_() {
  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sig_int_fd))
    qFatal("Couldn't create INT socketpair");

  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sig_term_fd))
    qFatal("Couldn't create TERM socketpair");

  sn_int = new QSocketNotifier(sig_int_fd[1], QSocketNotifier::Read, this);
  connect(sn_int, &QSocketNotifier::activated, this, &OmPlayer::HandleSigInt);

  sn_term = new QSocketNotifier(sig_term_fd[1], QSocketNotifier::Read, this);
  connect(sn_term, &QSocketNotifier::activated, this, &OmPlayer::HandleSigTerm);

  MainWindow* main_window_ = new MainWindow();

  main_window_->SetupWindow();
  main_window_->show();

  connect(main_window_, &MainWindow::onClose, this, &OmPlayer::Stop);
  connect(main_window_, &MainWindow::onKeyEscape, [&]() { ToggleFullScreen(false); });
  connect(this, &OmPlayer::onToggleFullScreen, main_window_, &MainWindow::FullScreen);
  connect(this, &OmPlayer::onToggleFullScreen, this, &OmPlayer::ToggleFullScreen);
  connect(this, &OmPlayer::onToggleLoopMode, this, &OmPlayer::ToggleLoopMode);
  connect(this, &OmPlayer::onToggleMute, this, &OmPlayer::ToggleMute);
  connect(this, &OmPlayer::onPlayBackStarted, main_window_, &MainWindow::Show);
  connect(this, &OmPlayer::onPlaybackStopped, main_window_, &MainWindow::Hide);
  connect(this, &OmPlayer::onSetPosistion, this, &OmPlayer::SetPosistion);

  instance_ = std::make_unique<VLC::Instance>(0, nullptr);

  if (instance_) {
    base_player_ = std::make_shared<VLC::MediaPlayer>(*instance_);
    play_list_ = std::make_shared<VLC::MediaList>(*instance_);
    player_ = std::make_shared<VLC::MediaListPlayer>(*instance_);
  } else
    throw std::runtime_error("vlc engine creation failed");

  auto& files = opts_.GetFiles();

  if (opts_.GetFiles().empty())
    throw std::runtime_error(fmt::format("empty directory: {}", opts_.Get().at("source").get<std::string>()));
  else
    LOG(DEBUG) << "total files: " << files.size();

  for (size_t i = 0; i < files.size(); i++) {
    auto media = VLC::Media(*instance_, files[i], VLC::Media::FromPath);
    play_list_->addMedia(media);
    LOG(DEBUG) << "index " << i << ": " << std::filesystem::path(files[i]).filename();
  }

  for (auto& file_path : opts_.GetFiles()) {
    auto media = VLC::Media(*instance_, file_path, VLC::Media::FromPath);
  }

  player_->setMediaPlayer(*base_player_);
  player_->setMediaList(*play_list_);

  base_player_->setMute(false);
  base_player_->setXwindow(main_window_->GetWidgetId());

  base_player_->eventManager().onMediaChanged([&](VLC::MediaPtr media) {
    current_pos_ = 0;
    emit onPlayBackStarted();

    LOG(DEBUG) << "playing: " << media->mrl();
  });

  base_player_->eventManager().onTimeChanged([&](libvlc_time_t pos) { current_pos_ = pos; });

  base_player_->eventManager().onStopped([&]() {
    emit onPlaybackStopped();

    LOG(DEBUG) << "playback stopped";
  });

  play_list_->eventManager().onEndReached([&]() { LOG(DEBUG) << "playlist finished"; });
}

OmPlayer::~OmPlayer() {
  if (player_thread_ && player_thread_->joinable())
    player_thread_->join();
}

void OmPlayer::Stop() {
  if (player_->isPlaying())
    player_->stop();

  if (controller_)
    controller_->Stop();

  io_.Stop();
  quit();
}

void OmPlayer::GpioSwitch(int clip_index, int state) {
  VLC::MediaPtr media = nullptr;
  int media_index = -1;
  bool is_playing = player_->isPlaying();

  if (is_playing)
    media = base_player_->media();

  if (media != nullptr)
    media_index = play_list_->indexOfItem(*media);

  if (state == 0 && is_playing && media_index == clip_index)
    player_->stop();

  else if (state == 1) {
    player_->playItemAtIndex(clip_index);
    emit onToggleLoopMode(2);
  }
}

void OmPlayer::TermSignalHandler(int) {
  char a = 1;
  ::write(sig_term_fd[0], &a, sizeof(a));
}

void OmPlayer::IntSignalHandler(int) {
  char a = 1;
  ::write(sig_int_fd[0], &a, sizeof(a));
}

void OmPlayer::HandleSigInt() {
  sn_int->setEnabled(false);
  char tmp;
  ::read(sig_int_fd[1], &tmp, sizeof(tmp));

  Stop();
}

void OmPlayer::HandleSigTerm() {
  sn_term->setEnabled(false);
  char tmp;
  ::read(sig_term_fd[1], &tmp, sizeof(tmp));

  Stop();
}

void OmPlayer::ParseCommand(const std::string& cmd) {
  std::string token;
  std::istringstream token_stream(cmd);
  std::vector<std::string> tokens;

  while (std::getline(token_stream, token, ';'))
    tokens.push_back(token);

  if (tokens.size() != 3 || tokens[0][0] != '#') {
    LOG(ERROR) << "wrong command: " << cmd;
    return;
  }

  std::string id = tokens[0];
  id.erase(std::begin(id));

  try {
    if (opts_["id"] == std::stoi(id))
      Control(tokens[1], tokens[2]);
  } catch (std::invalid_argument&) {
    LOG(ERROR) << "invalid pi id: " << tokens[0];
  }
}

void OmPlayer::Control(const std::string& cmd, const std::string& arg) {
  try {
    switch (_hash(cmd.c_str())) {
      case "stop"_:
        player_->stop();
        break;
      case "play"_:
        player_->play();
        break;
      case "next"_:
        player_->next();
        break;
      case "prev"_:
        player_->previous();
        break;
      case "exit"_:
        Stop();
        break;
      case "loop"_: {
        emit onToggleLoopMode(std::stoi(arg));
        break;
      }
      case "full"_: {
        emit onToggleFullScreen(std::stoi(arg));
        break;
      }
      case "seek"_: {
        emit onSetPosistion(std::stoi(arg));
        break;
      }
      case "mute"_: {
        emit onToggleMute(std::stoi(arg));
        break;
      }
      case "rate"_: {
        base_player_->setRate(std::stof(arg));
        break;
      }
      case "goto"_: {
        player_->playItemAtIndex(std::stoi(arg));
        break;
      }
      case "pause"_: {
        base_player_->pause();
        break;
      }
      case "show"_: {
        emit onPlayBackStarted();
        break;
      }
      case "hide"_: {
        emit onPlaybackStopped();
        break;
      }
      case "status"_: {
        std::string media_mrl;
        if (player_->isPlaying())
          media_mrl = base_player_->media()->mrl();

        LOG(INFO) << "status: " << kStatus[static_cast<size_t>(player_->state())] << " " << media_mrl;
        break;
      }
      case "version"_: {
        LOG(INFO) << *this;
        break;
      }
      default:
        LOG(ERROR) << "unknown command: " << cmd;
        break;
    }
  } catch (std::invalid_argument&) {
    LOG(ERROR) << "wrong argument: " << arg;
  }
}

void OmPlayer::StartInSerialMode() {
  controller_ = std::make_shared<SerialPort>(io_);
  controller_->Init(opts_["serial"]["device"], opts_["serial"]["speed"]);
  controller_->Start([&](std::vector<char>& res, ssize_t len) {
    LOG(INFO) << "check";

    std::string command(res.data(), static_cast<size_t>(len));
    std::istringstream input(command);

    while (input >> command)
      ParseCommand(command);
  });
}

void OmPlayer::StartInGpioMode() {
  std::vector<int> pins;
  for (auto& bcm : opts_.Get()["gpio"])
    pins.push_back(bcm);

  controller_ = std::make_shared<GPIO>();
  controller_->Init(pins);
  controller_->Start([&](int pin, int mode) { GpioSwitch(pin, mode); });
}

void OmPlayer::ToggleLoopMode(int mode) {
  if (mode > -1 && mode < 3) {
    player_->setPlaybackMode(static_cast<libvlc_playback_mode_t>(mode));

    LOG(INFO) << "loop mode: " << std::boolalpha << mode;
  } else
    LOG(ERROR) << "wrong loop mode: " << mode << ". (must be 0..2)";
}

void OmPlayer::ToggleMute(bool mute) {
  base_player_->setMute(mute);

  LOG(INFO) << "mute volume: " << std::boolalpha << mute;
}

void OmPlayer::SetPosistion(int pos) {
  if (!base_player_->isSeekable() || !base_player_->isPlaying()) {
    LOG(WARN) << "video is not seekable";
    return;
  }

  libvlc_time_t current_dur_ = base_player_->media()->duration();

  base_player_->setPosition(static_cast<float>(current_pos_ + (pos * 1000)) / current_dur_);
  LOG(INFO) << "seek: " << pos << 's';
}

void OmPlayer::ToggleFullScreen(bool fs) {
  base_player_->setFullscreen(fs);
  LOG(DEBUG) << "full screen mode: " << std::boolalpha << fs;
}

void OmPlayer::Start() {
  mode_ = opts_["mode"];

  io_.Init();

  if (mode_ == ControlMode::SERIAL)
    StartInSerialMode();
  else if (mode_ == ControlMode::GPIO)
    StartInGpioMode();
  else
    throw(std::logic_error(fmt::format("unknown mode: {}", mode_)));

  if (opts_.Find("autoplay") != opts_.End() && opts_["autoplay"])
    player_->play();

  if (opts_.Find("fullscreen") != opts_.End() && opts_["fullscreen"])
    emit onToggleFullScreen(true);
  else
    emit onToggleFullScreen(false);

  player_thread_ = std::make_unique<std::thread>([&]() { io_.Run(); });

  exec();
}

}  // namespace omega
