#include "io/io.h"

#include <csignal>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include <sys/eventfd.h>
#include <unistd.h>

#include "io/task.h"
#include "utils/logger.h"

namespace omega {

Io::Io() : queue_(), stopped_(false) {}

void Io::Init() {
  epoll_fd_ = epoll_create1(0);

  if (epoll_fd_ == -1)
    throw std::runtime_error("create epoll file descriptor failed");

  close_fd_ = eventfd(0, EFD_NONBLOCK);

  if (close_fd_ == -1)
    throw std::runtime_error("create close_event file descriptor failed");

  epoll_event close_ev;

  close_ev.data.fd = close_fd_;
  close_ev.events = EPOLLIN | EPOLLET;

  if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, close_ev.data.fd, &close_ev) != 0)
    throw std::runtime_error("set close event failed");

  queue_.Start(2);
}

Io::~Io() {
  close(epoll_fd_);
}

void Io::Run() {
  std::vector<epoll_event> events(EPOLL_MAX_EVENTS);

  while (!stopped_) {
    int ready = epoll_wait(epoll_fd_, &events[0], EPOLL_MAX_EVENTS, -1);

    if (ready == -1)
      throw std::runtime_error("io epoll_wait failed");

    for (int i = 0; i < ready; i++)
      Handle(events[i]);

    events.clear();
  }

  queue_.Stop();
}

void Io::Stop() {
  eventfd_write(close_fd_, 1);
}

void Io::Register(Handler::Ptr handler) {
  int fd = handler->e.data.fd;
  int err = epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, fd, &(handler->e));

  if (err != 0)
    throw std::runtime_error("io epoll_ctl failed");

  handlers_[fd] = handler;
}

void Io::Handle(epoll_event& ev) {
  int fd = ev.data.fd;

  if (fd == close_fd_ && ev.events & EPOLLIN)
    stopped_ = true;

  if (handlers_.count(fd) == 0)
    return;

  auto handler = handlers_[fd];

  if (handler != nullptr) {
    auto task = std::async(std::launch::deferred, &Handler::Exec, handler, std::move(ev));
    queue_.Add(std::move(task));
  }
}

void Io::Release(int fd) {
  int err = epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, fd, nullptr);

  if (err != 0)
    throw std::runtime_error("io epoll_ctl failed: " + std::string(std::strerror(errno)));

  if (handlers_.count(fd))
    handlers_.erase(fd);
}

}  // namespace omega
