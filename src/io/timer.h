#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <memory>

#include <fcntl.h>
#include <sys/timerfd.h>

#include "handler.h"
#include "io.h"

class Timer {
 public:
  using Ptr = std::unique_ptr<Timer>;

  Timer(Io& io);
  ~Timer() {
    close(fd_);
  }

  void AsyncWait(TimerHandler::Sig&& func, const std::chrono::seconds& delay);

 private:
  Io& io_;
  int fd_;

  void SetTimer(const std::chrono::seconds& delay);
};

#endif  // TIMER_H
