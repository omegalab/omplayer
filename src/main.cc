#include <csignal>
#include <iostream>

#include "player/player.h"
#include "utils/logger.h"
#include "utils/options.h"
#include "utils/runguard.h"

static int SetupUnixSignalHandlers();

int main(int argc, char* argv[]) {
  Logger::Instance().init(10);

  RunGuard guard("omplayer");
  if (!guard.tryToRun()) {
    LOG(ERROR) << "already running";
    return EXIT_FAILURE;
  }

  if (SetupUnixSignalHandlers() != 0)
    LOG(ERROR) << "failed setup unix signal handlers";

  try {
    omega::Options options(argc, argv);
    options.Validate();

    omega::OmPlayer omplayer(argc, argv, options);
    omplayer.Start();
  } catch (std::runtime_error& e) {
    LOG(ERROR) << PROJECT_NAME << " version: " << PROJECT_VER << " build date: " << PROJECT_BUILD_DATE;
    LOG(ERROR) << e.what();
    return EXIT_FAILURE;
  } catch (std::exception& e) {
    LOG(ERROR) << e.what();
  }

  return EXIT_SUCCESS;
}

int SetupUnixSignalHandlers() {
  struct sigaction sig_int, sig_term;

  sig_int.sa_handler = omega::OmPlayer::IntSignalHandler;
  sigemptyset(&sig_int.sa_mask);
  sig_int.sa_flags = 0;
  sig_int.sa_flags |= SA_RESTART;

  if (sigaction(SIGINT, &sig_int, nullptr))
    return 1;

  sig_term.sa_handler = omega::OmPlayer::TermSignalHandler;
  sigemptyset(&sig_term.sa_mask);
  sig_term.sa_flags |= SA_RESTART;

  if (sigaction(SIGTERM, &sig_term, nullptr))
    return 2;

  return 0;
}
